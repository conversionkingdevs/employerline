/**************************************
           LOAD EXTERNAL SCRIPTS
**************************************/
function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
      if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
        callback.done = true;
        callback();
      }
    };
    document.querySelector('head').appendChild(s);
  };
  
  function defer(method, selector) {
      if (window.jQuery) {
          if (jQuery(selector).length > 0){
              method();  
          } else {
              setTimeout(function() { defer(method, selector); }, 50);
          }  
      } else {
           setTimeout(function() { defer(method, selector); }, 50);
      }    
  }
  
/** Flexslider **/
defer(function () {
    jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />')
    }, 'head');

getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.min.js",start_flexslider);
        
function start_flexslider(){
    defer(function () {
        if(jQuery(window).width() < 767 && jQuery(".testimonials-landing").length > 0){
            jQuery('body').addClass('opt7-v2');
        };

        /**************************************
            LAYOUT V2
        **************************************/ 
        jQuery('.testimonials-landing .row > .medium-6').removeClass('medium-6').addClass('medium-5');
        jQuery(".opt7-v2 .testimonials-landing .row .large-3").wrapAll('<div class="reviews-carousel flexslider"><ul class="slides"></ul></div>');
        var $elements = jQuery(".reviews-carousel .large-3");
        for (var i = 0; i < $elements.length; i += 2) {
            $elements.slice(i, i + 2).wrapAll('<li></li>');
        };

        jQuery('.reviews-carousel').flexslider({
            directionNav: true,
            controlNav: true,
            smoothHeight: true,
            animation: "slide"
        });
    }, '.testimonials-landing');
};