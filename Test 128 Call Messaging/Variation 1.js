function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function getTitles () {
    var title = {
            "header":  "Employment Relations Act",
            "area": "Employment Relations Act",
            "the": ""
        },
        url = document.location.pathname;

    switch (url) {
        case "/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/era/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/minimum-wage/":
            title.header = "Minimum Wage";
            title.area = "Minimum Wages";
            break;
        case "/employment-relations/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/pay-rates/":
            title.header = "Pay Rate";
            title.area = "Pay Rates";
            break;
        case "/employment-law/":
            title.header = "Employment Law";
            title.area = "Employment Law";
            break;
        case "/employment-contract/":
            title.header = "Employment Contract";
            title.area = "Employment Contracts";
            break;
        case "/wage-rates/":
            title.header = "Wage Rate";
            title.area = "Wage Rates";
            break;
        case "/employer-obligations/":
            title.header = "Employer Obligations";
            title.area = "Employer Obligations";
            break;
        case "/sick-leave/":
            title.header = "Sick Leave";
            title.area = "Sick Leave";
            break;
        case "/employment-nz/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/maternity-leave/":
            title.header = "Maternity Leave";
            title.area = "Maternity Leave";
            break;
        case "/wages-rates-return/":
            title.header = "Wage Rate";
            title.area = "Wage Rates";
            break;
        case "/labour-department/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/redundancy/":
            title.header = "Redundancy";
            title.area = "Redundancy";
            break;
        case "/agreements/":
            title.header = "Agreement";
            title.area = "Agreements";
            break;
        case "/termination/":
            title.header = "Termination";
            title.area = "Termination";
            break;
        case "/workplace-legislation/":
            title.header = "Workplace Legislation";
            title.area = "Workplace Legislation";
            break;
        case "/salary-advice/":
            title.header = "Salary Advice";
            title.area = "Salary Advice";
            break;
        case "/employee-leave/":
            title.header = "Employee Leave";
            title.area = "Employee Leave";
            break;
        case "/workplace-discrimination/":
            title.header = "Workplace Discrimination";
            title.area = "Workplace Discrimination";
            break;
        case "/apprenticeship-obligations/":
            title.header = "Apprenticeship Obligation";
            title.area = "Apprenticeship Obligations";
            break;
        case "/employment-contract/":
            title.header = "Employment Contract";
            title.area = "Employment Contracts";
            break;
        case "/allowance/":
            title.header = "Allowance";
            title.area = "Allowances";
            break;
        case "/disciplinary-actions/":
            title.header = "Disciplinary Action";
            title.area = "Disciplinary Action";
            break;
            
        case "/dismissal/":
            title.header = "Dismissal";
            title.area = "Dismissal";
            break;
        case "/personal-grievance/":
            title.header = "Personal Grievance";
            title.area = "Personal Grievances";
            break;
        case "/workplace-bullying/":
            title.header = "Workplace Bullying";
            title.area = "Workplace Bullying";
            break;
        case "/workplace-diversity/":
            title.header = "Workplace Diversity";
            title.area = "Workplace Diversity";
            break;
        case "/bullying/":
            title.header = "Workplace Bullying";
            title.area = "Workplace Bullying";
            break;
        case "/workplace-diversity/":
            title.header = "Workplace Diversity";
            title.area = "Workplace Diversity";
            break;
        case "/flexible-workplace/":
            title.header = "Flexible Workplace";
            title.area = "Flexible Workplaces";
            break;
        case "/allowances/":
            title.header = "Allowance";
            title.area = "Allowances";
            break;
        case "/public-holidays/":
            title.header = "Public Holiday";
            title.area = "Public Holidays";
            break;
        case "/health-and-safety/":
            title.header = "Health and Safety";
            title.area = "Health and Safety";
            break;
        case "/overtime/":
            title.header = "Overtime";
            title.area = "Overtime";
            break;
        case "/workplace-diversity/":
            title.header = "Workplace Diversity";
            title.area = "Workplace Diversity";
            break;
        case "/annual-leave/":
            title.header = "Annual Leave";
            title.area = "Annual Leave";
            break;
        default: 
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;

    }

    return title;

}

function start() {
    var title = getTitles();
    var banner = jQuery('.empl_center');
    banner.find('> .mobile-header').remove();
    if (document.location.path == 'salary-advice') {
        banner.find('.hidefrommobile').html('We can help with you with '+title.the+' <br><strong>'+title.area+'</strong><br><span class="opt_SmallText">Call Our Toll Free 24 Hour Helpline <strong>For Business Owners</strong>');
    } else {
        banner.find('.hidefrommobile').html('We can advise in all areas of '+title.the+' <br><strong>'+title.area+'</strong><br><span class="opt_SmallText">Call Our Toll Free 24 Hour Helpline <strong>For Business Owners</strong>');
    }
    jQuery('.readytocall .togglemodal.hidefromtable').text('Send Online Enquiry');
}
defer(start,'.empl_center');