function addHtml(html) {
    jQuery('body').append(html);
}

function addPhoneNumber(el) {
    el.html(jQuery('.mobile-header').clone());
}

function addToggleFixedContainer(toggle, container) {
    jQuery(toggle).click(function () {
        jQuery(container).toggleClass('opt_hide');
        jQuery(toggle).toggleClass('down');
        jQuery(toggle).toggleClass('up');
    });
}

function start() {
    var text = 'Call Our Free 24/7 Help Line <br> For Businesses & Employers',
        html = '<div class="opt_FixedCallContainer"> <div class="opt_arrowContainer up"> <p class="opt_arrow"></p></div><div class="opt_fixedCallContainerContent opt_hide"> <p>' + text + '</p><div class="opt_callButton"> <p>Call <span class="opt_number"></span></p></div></div></div>';
    addHtml(html);
    addPhoneNumber(jQuery('.opt_FixedCallContainer .opt_number'));
    addToggleFixedContainer('.opt_arrowContainer', '.opt_fixedCallContainerContent');
    jQuery('.opt_callButton').click(function (e) {
        e.stopPropagation();
        jQuery(this).find('a').click();
    });
    jQuery('.opt_callButton a').click(function (e) {
        e.stopPropagation();
    });
    jQuery(window).scroll(function () {
        if (!jQuery('body').hasClass('opt_scrolled')) {
            if (jQuery(window).scrollTop() > 230) {
                jQuery('.opt_fixedCallContainerContent').toggleClass('opt_hide');
                jQuery('.opt_arrowContainer').toggleClass('down');
                jQuery('.opt_arrowContainer').toggleClass('up');
                jQuery('body').addClass('opt_scrolled');
            }
        }
    });
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0) {
            method();
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    } else {
        setTimeout(function () {
            defer(method, selector);
        }, 50);
    }
}

defer(start, '.mobile-header');