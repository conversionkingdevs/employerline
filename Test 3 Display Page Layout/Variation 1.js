function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function getTitles () {
    var title = {
            "header":  "Employment Relations Act",
            "area": "Employment Relations Act",
            "the": ""
        },
        url = document.location.pathname;

    switch (url) {
        case "/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/era/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/minimum-wage/":
            title.header = "Minimum Wage";
            title.area = "Minimum Wages";
            break;
        case "/employment-relations/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/pay-rates/":
            title.header = "Pay Rate";
            title.area = "Pay Rates";
            break;
        case "/employment-law/":
            title.header = "Employment Law";
            title.area = "Employment Law";
            break;
        case "/employment-contract/":
            title.header = "Employment Contract";
            title.area = "Employment Contracts";
            break;
        case "/wage-rates/":
            title.header = "Wage Rate";
            title.area = "Wage Rates";
            break;
        case "/employer-obligations/":
            title.header = "Employer Obligations";
            title.area = "Employer Obligations";
            break;
        case "/sick-leave/":
            title.header = "Sick Leave";
            title.area = "Sick Leave";
            break;
        case "/employment-nz/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/maternity-leave/":
            title.header = "Maternity Leave";
            title.area = "Maternity Leave";
            break;
        case "/wages-rates-return/":
            title.header = "Wage Rate";
            title.area = "Wage Rates";
            break;
        case "/labour-department/":
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;
        case "/redundancy/":
            title.header = "Redundancy";
            title.area = "Redundancy";
            break;
        case "/agreements/":
            title.header = "Agreement";
            title.area = "Agreements";
            break;
        case "/termination/":
            title.header = "Termination";
            title.area = "Termination";
            break;
        case "/workplace-legislation/":
            title.header = "Workplace Legislation";
            title.area = "Workplace Legislation";
            break;
        case "/salary-advice/":
            title.header = "Salary Advice";
            title.area = "Salary Advice";
            break;
        case "/employee-leave/":
            title.header = "Employee Leave";
            title.area = "Employee Leave";
            break;
        case "/workplace-discrimination/":
            title.header = "Workplace Discrimination";
            title.area = "Workplace Discrimination";
            break;
        case "/apprenticeship-obligations/":
            title.header = "Apprenticeship Obligation";
            title.area = "Apprenticeship Obligations";
            break;
        case "/employment-contract/":
            title.header = "Employment Contract";
            title.area = "Employment Contracts";
            break;
        case "/allowance/":
            title.header = "Allowance";
            title.area = "Allowances";
            break;
        case "/disciplinary-actions/":
            title.header = "Disciplinary Action";
            title.area = "Disciplinary Action";
            break;
            
        case "/dismissal/":
            title.header = "Dismissal";
            title.area = "Dismissal";
            break;
        case "/personal-grievance/":
            title.header = "Personal Grievance";
            title.area = "Personal Grievances";
            break;
        case "/workplace-bullying/":
            title.header = "Workplace Bullying";
            title.area = "Workplace Bullying";
            break;
        case "/workplace-diversity/":
            title.header = "Workplace Diversity";
            title.area = "Workplace Diversity";
            break;
        case "/bullying/":
            title.header = "Workplace Bullying";
            title.area = "Workplace Bullying";
            break;
        case "/workplace-diversity/":
            title.header = "Workplace Diversity";
            title.area = "Workplace Diversity";
            break;
        case "/flexible-workplace/":
            title.header = "Flexible Workplace";
            title.area = "Flexible Workplaces";
            break;
        case "/allowances/":
            title.header = "Allowance";
            title.area = "Allowances";
            break;
        case "/public-holidays/":
            title.header = "Public Holiday";
            title.area = "Public Holidays";
            break;
        case "/health-and-safety/":
            title.header = "Health and Safety";
            title.area = "Health and Safety";
            break;
        case "/overtime/":
            title.header = "Overtime";
            title.area = "Overtime";
            break;
        case "/workplace-diversity/":
            title.header = "Workplace Diversity";
            title.area = "Workplace Diversity";
            break;
        case "/annual-leave/":
            title.header = "Annual Leave";
            title.area = "Annual Leave";
            break;
        default: 
            title.header = "Employment Relations Act";
            title.area = "Employment Relations Act";
            title.the = "the ";
            break;

    }

    return title;

}  

defer(function () {

    var title = getTitles();

    var html = ' <section id="opt_newBanner"> <div class="row"> <div class="opt_Header"> <div class="opt_Logo"> <img src="https://employerline.co.nz/wp-content/themes/fairworkhelp/img/fm_images/logo-header.png"> </div><div class="opt_Information"> <div class="opt_Row"> <a class="opt_Phone" href=""> <p class="opt_GreenDot">24/7</p><p><span class="opt_Number"></span></p></a> </div><div class="opt_Row"> <p class="opt_SmallText">Free '+title.header+' Helpline for Businesses</p></div></div></div><div class="opt_Bottom"> <div class="opt_Half"> <div class="opt_TopText"> <p class="opt_LargeText">We can advise in all areas of '+title.the+'<br><span id="opt_Page" class="opt_Bold opt_LargeText">'+title.area+'</span> </p><p class="opt_SmallText">Toll Free 24 Hour Helpline <strong>For Business Owners</strong></p></div><div class="opt_Button"> <a href="" class="opt_Phone"> <p class="opt_GreenDot"><span class="opt_CenterLine">Toll Free</span><span class="opt_Number"></span></p></a> </div><div class="opt_BottomText"> <p class="opt_SmallText">Giving over 12,000 businesses like yours peace of mind.</p><p class="opt_Bold">Employsure is rated <span class="opt_Stars"></span> based on 532 reviews</p></div></div></div></div></section>'

    var formhtml = '<section class="empl_secondsection" id="opt_Form"> <div class="row"> <div class="large-12 column"> <div class="row"> <div class="large-12 column"> <h3 class="text-center opt_FormHeading"> FREE <span class="opt_PageTitle"> '+title.header+'</span> Advice <strong>for business owners</h3> </div></div><div class="row"> <div class="large-12 column"> <div class="opt_Box"> <div class="opt_FormBoxHeading"> <p>Contact us by phone or email</p></div><div class="opt_FormBottom"> <div class="opt_Half opt_Left"> <img src="https://employerline.co.nz/wp-content/themes/fairworkhelp/img/steps-1.png"> <div class="opt_Button"> <a href="" class="opt_Phone"> <p class="opt_GreenDot"><span class="opt_CenterLine">Toll Free</span><span class="opt_Number"></span></p></a> </div><div class="opt_FormSubText"> <p>Advisors ready to take your call 24/7</p><p class="opt_MobileText">Complete the form and we’ll call you back ASAP</p></div></div><div class="opt_Half opt_Right"> </div></div></div></div></div></div></div></section>'

    function updatePhoneNumbers (number) {
        jQuery('.opt_Number').text(number);
        jQuery('a.opt_Phone').attr('href', 'tel:'+number);
    }

    window.optPhoneNumber = jQuery('.mobile-header a').text();

    updatePhoneNumbers(optPhoneNumber);

    function checkPhone () {
        checkagainst = jQuery('.mobile-header').html().split(',')[1].split(')')[0].replace(/\'/g,'');

        if (window.optPhoneNumber != checkagainst) {
            window.optPhoneNumber = jQuery('.mobile-header a').text();
            updatePhoneNumbers(optPhoneNumber);
        }
    }

    function checkPhoneNotEmpty () {
        var check = true;
        checkPhone();
        jQuery('.opt_Number').each(function () {
            if (jQuery(this).text().length < 1) {
                check = false;
            }
        });
        if (check == false) {
            setTimeout(function () {
                checkPhoneNotEmpty();
            }, 100)
        }
    }

    document.getElementsByClassName('mobile-header')[0].addEventListener("DOMSubtreeModified", function (event) {
        checkPhone();
    });

    jQuery('body').prepend(html);

    jQuery('.we-can-help li h4').each(function () {
        jQuery(this).text(jQuery(this).text().toLowerCase())
    })

    // Replace the Testimonials Heading
    jQuery('.testimonials-landing .row .text-center strong').text(jQuery('.section-steps .row .text-center').text().trim()).addClass('opt_TestimonialsHeading');

    jQuery('.section-steps').after(formhtml);
    jQuery('.modal-form-popup').appendTo('.opt_Half.opt_Right');

    checkPhoneNotEmpty();

}, '.empl_header .empl_center .hidefrommobile');